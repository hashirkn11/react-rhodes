import React, {useState, useEffect} from 'react';
import {ReactDOM, render } from 'react-dom';
import './css/main.css';
import './css/media.css';
import './css/media.css';
import Nav from './Nav';
import Footer from './Footer'
import { Story } from './Story';
import { Home } from './Home';
import { Contact } from './Contact';
import {
  BrowserRouter,
  Routes,
  Route,
} from "react-router-dom";

let URL = 'https://reqres.in/api/users';


render(
    <BrowserRouter>
      <Nav/>
        <Routes>
          <Route path="/" element={<Home title="Home" />}></Route>
          <Route path="/story" element={<Story title="Success stories | Home" />} />
          <Route path="/contact" element={<Contact title="Contact | Home" />} />
            {/* <Route path="teams" element={<Teams />}>
              <Route path=":teamId" element={<Team />} />
              <Route path="new" element={<NewTeamForm />} />
              <Route index element={<LeagueStandings />} />
            </Route> */}
        </Routes>
      <Footer/>
  </BrowserRouter>,
  document.getElementById("root")
  );


// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
// reportWebVitals();
