import {React, useState, useEffect} from 'react'

export const Contact = ({title}) => {
    let [Dtitle, setTitle] = useState('')
    document.querySelector('title').textContent = Dtitle;
    useEffect(() => {
        setTitle(title)
    })
    return (
        <>
            <div className="contact_header flex-center sm-header">
                <h1 className="hero_title">Case Evaluation</h1>
            </div>

            <main className="contact_us_main">
                <div className="container">
                    <div className="row">
                        <div className="col-md-6">
                            <div className="text-content">
                                <h2 className="title">We are eager to hear from you and provide a free evaluation of your claim.
                                </h2>
                                <p className="para"><b>Please fill out the free case evaluation form and we will be in touch after
                                    reviewing the information you provide.</b></p>
                                <p className="para">As always, your consultation is confidential and privileged, but please be aware
                                    that no attorney-client relationship is yet formed by this exchange of information. In other
                                    words, we will provide you with a free evaluation of your claim, but we do not represent you
                                    yet, even if you send in the information. Should we feel that we can successfully prosecute
                                    a claim on your behalf, we will provide you with a written engagement letter outlining the
                                    terms of the representation.
                                </p>
                                <p className="para">Thank you again for your confidence in us. We look forward to working with you.
                                    And please remember, all conversations are confidential for your protection.
                                </p>
                                <p className="para">You may also call us at 770-643-1606</p>

                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="form">
                                <form>
                                    <h2 className="section_title text-center">Case Evaluation</h2>
                                    <div className="input-group">
                                        <input type="text" className="form-control ms-0" placeholder="Name" />
                                        <input type="text" className="form-control me-0" placeholder="Company Name" />
                                    </div>
                                    <div className="input-group">
                                        <input type="email" className="form-control ms-0" placeholder="Email" />
                                        <input type="number" className="form-control me-0" placeholder="Phone" />
                                    </div>
                                    <textarea name="message" className="form-control" placeholder="Message"></textarea>
                                    <div className="option">
                                        <p className="text">Have you been retaliated against in any way with regard to this matter?</p>
                                        <div className="input-group">
                                            <div className="form-check me-3">
                                                <input className="form-check-input" type="radio" name="flexRadio1" id="flexRadioDefault1" />
                                                <label className="form-check-label" htmlFor="flexRadioDefault1" >
                                                    Yes
                                                </label>
                                            </div>
                                            <div className="form-check">
                                                <input className="form-check-input" type="radio" name="flexRadio1" id="flexRadioDefault2" checked />
                                                <label className="form-check-label" htmlFor="flexRadioDefault2">
                                                    No
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="option">
                                        <p className="text">Do you work where the fraud is being perpetrated?</p>
                                        <div className="input-group">
                                            <div className="form-check me-3">
                                                <input className="form-check-input" type="radio" name="flexRadioDefault2" id="flexRadioDefault1" />
                                                <label className="form-check-label" htmlFor="flexRadioDefault1">
                                                    Yes
                                                </label>
                                            </div>
                                            <div className="form-check">
                                                <input className="form-check-input" type="radio" name="flexRadioDefault2" id="flexRadioDefault2" checked />
                                                <label className="form-check-label" htmlFor="flexRadioDefault2">
                                                    No
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="option">
                                        <p className="text">Did you make an internal complaint about the fraud to your employer?</p>
                                        <div className="input-group">
                                            <div className="form-check me-3">
                                                <input className="form-check-input" type="radio" name="flexRadioDefault3" id="flexRadioDefault1" />
                                                <label className="form-check-label" htmlFor="flexRadioDefault1">
                                                    Yes
                                                </label>
                                            </div>
                                            <div className="form-check">
                                                <input className="form-check-input" type="radio" name="flexRadioDefault3" id="flexRadioDefault2" checked />
                                                <label className="form-check-label" htmlFor="flexRadioDefault2">
                                                    No
                                                </label>
                                            </div>
                                        </div>
                                    </div>
                                    <a href="#" className="btn d-block text-center">Free case Evaluation</a>
                                </form>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </>
    )
}
