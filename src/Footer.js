import react from 'react';

function Footer() {
    return (
        <footer>
            <div className="container">
                <div className="logo text-center">
                    <img src="img/logo.png" alt="logo" />
                    <div className="socials d-flex justify-content-center">
                        <a href="#" className="social_link"><i className="fab fa-facebook-f" /></a>
                        <a href="#" className="social_link"><i className="fab fa-instagram" /></a>
                        <a href="#" className="social_link"><i className="fab fa-youtube" /></a>
                    </div>
                </div>
                <div className="row">
                    <div className="col-md-6">
                        <div className="left_content">
                            <p className="para">1100 Peachtree ST NE <br />
                                Suite 200, Atlanta GA 30309</p>
                            <span className="note"><a href="#">Privicy Policy</a> | <a href="#">Terms &amp; Conditions</a>  |  © Copyright © 2021 Rodes</span>
                        </div>
                    </div>
                    <div className="col-md-6">
                        <div className="right_content text-end">
                            <div className="info">
                                <p className="txt"><span className="num">404. 605. 9680</span> <span className="icon"><i className="fas fa-phone-alt" /></span></p> <br />
                                <p className="txt"><span className="num">020 3441 3550</span> <span className="icon"><i className="fab fa-whatsapp" /></span></p> <br />
                                <p className="txt"><span className="num">info@rodes.com</span> <span className="icon"><i className="fas fa-envelope" /></span></p> <br />
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </footer>

    )
}

export default Footer;