import {React, useState, useEffect} from 'react'
// import './js/main';


export const Home = ({title}) => {
    let [Dtitle, setTitle] = useState('')
    document.querySelector('title').textContent = Dtitle;
    useEffect(() => {
        setTitle(title)
    })
    function openVideo(e) {
        e.preventDefault();
        document.querySelector('.video').classList.add('active')
    }
    function closeVideo(e) {
        e.preventDefault();
        document.querySelector('.video').classList.remove('active')
    }
    return (
        <>
            <div>
                <div className="header_section">
                    <div className="container">
                        <div className="content">
                            <h1 className="hero_title">Representing <br />
                                Qui Tam Whistleblowers <br />
                                Who Speak Up
                            </h1>
                            <a href="#" className="btn btn-golden-outline">Free case Evaluation</a>
                            <div className="arrow_down">
                                <a href="#main" className="down_btn mx-auto"><i className="fas fa-chevron-down" /></a>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="section_2" id="main">
                    <div className="container">
                        <div className="row justify-content-center">
                            <div className="col-md-12 col-lg-6 order-m-2">
                                <div className="text-content">
                                    <h2 className="title"><span className="text-golden">$500 <br />
                                        Million </span> <br />
                                        in the Last <br />
                                        10 Years</h2>
                                    <p className="paragraph">Since 1996, we have helped courageous whistleblowers recover government money through
                                        qui tam litigation, with a high ratio of successful cases and rewards for filers.
                                    </p>
                                    <p className="paragraph">If you believe you have a case and want an attorney experienced in qui tam law to take
                                        a look, please schedule a consultation right away. Our friendly team is always ready to offer advice where
                                        we can.
                                    </p>
                                </div>
                            </div>
                            <div className="col-md-8 col-lg-6 order-md-1">
                                <div className="frame_img_content position-relative">
                                    <img src="img/robert.jpg" className="person" alt="man" />
                                    <img src="img/bagde.jpg" className="img_badge" alt="img_badge" />
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="section_3">
                    <div className="container">
                        <div className="row">
                            <div className="col-md-6">
                                <div className="text-content">
                                    <img src="img/logo.png" alt="logo" className="logo" />
                                    <h2 className="title"><span className="text-golden">Whisper</span> <br /> before you <br /> <span className="text-golden">Whistle</span></h2>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="associated_with">
                                    <h3 className="box-title text-center">WORKED PREVIOUSLY WITH</h3>
                                    <div className="logos d-flex justify-content-between flex-wrap">
                                        <div className="logo">
                                            <img src="img/logo1.png" alt="img" />
                                        </div>
                                        <div className="logo">
                                            <img src="img/DOJ qui 1.png" alt="img" />
                                        </div>
                                        <div className="logo">
                                            <img src="img/DON 1.png" alt="img" />
                                        </div>
                                        <div className="logo">
                                            <img src="img/USNCIS 1.png" alt="img" />
                                        </div>
                                        <div className="logo">
                                            <img src="img/USSS 1.png" alt="img" />
                                        </div>
                                        <div className="logo">
                                            <img src="img/CID 1.png" alt="img" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="section_4">
                    <div className="container">
                        <h2 className="section_title text-center">Recognitions</h2>
                        <div className="brands">
                            <div className="row justify-content-center">
                                <div className="col-md-3 col-6 ms-0">
                                    <div className="brand">
                                        <img src="img/Image-28.png" alt="logo" />
                                    </div>
                                </div>
                                <div className="col-md-3 col-6 ms-0">
                                    <div className="brand">
                                        <img src="img/Image-19.png" alt="logo" />
                                    </div>
                                </div>
                                <div className="col-md-3 col-6 ms-0">
                                    <div className="brand">
                                        <img src="img/Image-22.png" alt="logo" />
                                    </div>
                                </div>
                                <div className="col-md-3 col-6 ms-0">
                                    <div className="brand me-0">
                                        <img src="img/Image-21.png" alt="logo" />
                                    </div>
                                </div>
                                <div className="col-md-3 col-6 ms-0">
                                    <div className="brand ms-0">
                                        <img src="img/Image-27.png" alt="logo" />
                                    </div>
                                </div>
                                <div className="col-md-3 col-6 ms-0">
                                    <div className="brand">
                                        <img src="img/Image-21.png" alt="logo" />
                                    </div>
                                </div>
                                <div className="col-md-3 col-6 ms-0">
                                    <div className="brand">
                                        <img src="img/Image-37.png" alt="logo" />
                                    </div>
                                </div>
                                <div className="col-md-3 col-6 ms-0">
                                    <div className="brand me-0">
                                        <img src="img/Image-19.png" alt="logo" />
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div className="our_focus">
                        <div className="container">
                            <div className="row">
                                <div className="col-lg-5">
                                    <div className="content">
                                        <h2 className="section_title">Our <br />
                                            Focus</h2>
                                    </div>
                                </div>
                                <div className="col-lg-7">
                                    <div className="content">
                                        <div className="text_box d-flex">
                                            <h3 className="heading">Qui Tam
                                                Wistleblowers</h3>
                                            <p className="para">Cases involving Medicare fraud, defense contractor fraud and other types of fraud
                                                against the federal government and states brought under the False Claims Act and similar state
                                                whistleblower laws.</p>
                                        </div>
                                        <div className="text_box d-flex">
                                            <h3 className="heading">SEC
                                                Wistleblowers</h3>
                                            <p className="para">Whistleblower claims involving securities law or Foreign Corrupt Practices Act
                                                violations that are filed with the Securities and Exchange Commission under the Dodd-Frank
                                                whistleblower reward program.</p>
                                        </div>
                                        <div className="text_box d-flex">
                                            <h3 className="heading">cftc
                                                Wistleblowers</h3>
                                            <p className="para">Whistleblower claims involving commodity law violations that are filed with the
                                                Commodity Futures Trading Commission under the whistleblower reward program created by the Dodd-Frank
                                                Act.</p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="section_5 d-none">
                    <div className="container">
                        {/* Swiper */}
                        <div className="swiper mySwiper">
                            <div className="swiper-wrapper">
                                <div className="swiper-slide">
                                    <div className="text-content text-center">
                                        <h2 className="nums text-golden"> 1000+</h2>
                                        <h3 className="caption">Admininstation <br /> mitigation letters written</h3>
                                    </div>
                                </div>
                                <div className="swiper-slide">
                                    <div className="text-content text-center">
                                        <h2 className="nums text-golden">1000+</h2>
                                        <h3 className="caption">Admininstation <br /> mitigation letters written</h3>
                                    </div>
                                </div>
                                <div className="swiper-slide">
                                    <div className="text-content text-center">
                                        <h2 className="nums text-golden">1000+</h2>
                                        <h3 className="caption">Admininstation <br /> mitigation letters written</h3>
                                    </div>
                                </div>
                                <div className="swiper-slide">
                                    <div className="text-content text-center">
                                        <h2 className="nums text-golden">1000+</h2>
                                        <h3 className="caption">Admininstation <br /> mitigation letters written</h3>
                                    </div>
                                </div>
                                <div className="swiper-slide">
                                    <div className="text-content text-center">
                                        <h2 className="nums text-golden">1000+</h2>
                                        <h3 className="caption">Admininstation <br /> mitigation letters written</h3>
                                    </div>
                                </div>
                            </div>
                            <div className="swiper-pagination" />
                        </div>
                    </div>
                </div>
                <div className="section_6 success_stories">
                    <div className="container">
                        <h2 className="section_title">Success <br /> Stories</h2>
                        <div className="row">
                            <div className="col-md-6">
                                <div className="left_content">
                                    <div className="success_story_blog">
                                        <div className="img">
                                            <div className="frame" />
                                            <img src="img/success_story_img.png" alt="img" />
                                        </div>
                                        <h3 className="title">Prime Healthcare, its founder, and doctor pay $37.5M in whistleblower kickbacks, Stark case</h3>
                                        <p className="des">Prime Healthcare, its founder and CEO, Dr. Prem Reddy, and a California cardiologist pay $37.5 million to resolve allegations of healthcare fraud. </p>
                                        <a href="#" className="link">learn more</a>
                                    </div>
                                    <div className="success_story_blog">
                                        <div className="img">
                                            <div className="frame" />
                                            <img src="img/success_story_img.png" alt="img" />
                                        </div>
                                        <h3 className="title">Prime Healthcare, its founder, and doctor pay $37.5M in whistleblower kickbacks, Stark case</h3>
                                        <p className="des">Prime Healthcare, its founder and CEO, Dr. Prem Reddy, and a California cardiologist pay $37.5 million to resolve allegations of healthcare fraud. </p>
                                        <a href="#" className="link">learn more</a>
                                    </div>
                                </div>
                            </div>
                            <div className="col-md-6">
                                <div className="right_content ms-0">
                                    <div className="success_story_blog">
                                        <div className="img">
                                            <div className="frame" />
                                            <img src="img/success_story_img.png" alt="img" />
                                        </div>
                                        <h3 className="title">Prime Healthcare, its founder, and doctor pay $37.5M in whistleblower kickbacks, Stark case</h3>
                                        <p className="des">Prime Healthcare, its founder and CEO, Dr. Prem Reddy, and a California cardiologist pay $37.5 million to resolve allegations of healthcare fraud. </p>
                                        <a href="#" className="link">learn more</a>
                                    </div>
                                    <div className="success_story_blog">
                                        <div className="img">
                                            <div className="frame" />
                                            <img src="img/success_story_img.png" alt="img" />
                                        </div>
                                        <h3 className="title">Prime Healthcare, its founder, and doctor pay $37.5M in whistleblower kickbacks, Stark case</h3>
                                        <p className="des">Prime Healthcare, its founder and CEO, Dr. Prem Reddy, and a California cardiologist pay $37.5 million to resolve allegations of healthcare fraud. </p>
                                        <a href="#" className="link">learn more</a>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <div className="section_7 flex-center">
                    <div className="video">
                        <a href="#" className="close_video d-block text-end m-2" onClick={(e) => closeVideo(e)}><i className="fas fa-times" /></a>
                        <iframe width="100%" height={315} src="https://www.youtube.com/embed/edAD0yrPTYo" title="YouTube video player" frameBorder={0} allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowFullScreen />
                    </div>
                    <div className="container">
                        <div className="text-content">
                            <h2 className="title">Pitblado Law is proud of our team during this unprecedented time.</h2>
                            <a href="#" className="btn play_video d-flex align-items-center p-0" onClick={(e) => openVideo(e)}><span className="icon_outline"><span className="icon flex-center"><i className="fas fa-play" /></span></span> Play Video</a>
                        </div>
                    </div>
                </div>
                <div className="section_8 d-none our_clients">
                    <div className="container">
                        <h2 className="section_title text-center">From Our
                            Clients </h2>
                        {/* Swiper */}
                        <div className="swiper mySwiper">
                            <div className="swiper-wrapper py-5">
                                <div className="swiper-slide">
                                    <div className="client_feedback">
                                        <span className="quote"><img src="img/quote.png" alt="icon" /></span>
                                        <p className="txt">During the first 30 years in my own business, my experience with attorneys was very limited. The only dealings I had with attorneys consisted of minor things, proper business licenses, adding or subtracting a paragraph in a contract, etc. That all changed in 2015. I can’t get into all of the detail</p>
                                        <p className="client_info">Johana Walker,  CEO OF ATLANTA</p>
                                    </div>
                                </div>
                                <div className="swiper-slide">
                                    <div className="client_feedback">
                                        <span className="quote"><img src="img/quote.png" alt="icon" /></span>
                                        <p className="txt">During the first 30 years in my own business, my experience with attorneys was very limited. The only dealings I had with attorneys consisted of minor things, proper business licenses, adding or subtracting a paragraph in a contract, etc. That all changed in 2015. I can’t get into all of the detail</p>
                                        <p className="client_info">Johana Walker,  CEO OF ATLANTA</p>
                                    </div>
                                </div>
                                <div className="swiper-slide">
                                    <div className="client_feedback">
                                        <span className="quote"><img src="img/quote.png" alt="icon" /></span>
                                        <p className="txt">During the first 30 years in my own business, my experience with attorneys was very limited. The only dealings I had with attorneys consisted of minor things, proper business licenses, adding or subtracting a paragraph in a contract, etc. That all changed in 2015. I can’t get into all of the detail</p>
                                        <p className="client_info">Johana Walker,  CEO OF ATLANTA</p>
                                    </div>
                                </div>
                                <div className="swiper-slide">
                                    <div className="client_feedback">
                                        <span className="quote"><img src="img/quote.png" alt="icon" /></span>
                                        <p className="txt">During the first 30 years in my own business, my experience with attorneys was very limited. The only dealings I had with attorneys consisted of minor things, proper business licenses, adding or subtracting a paragraph in a contract, etc. That all changed in 2015. I can’t get into all of the detail</p>
                                        <p className="client_info">Johana Walker,  CEO OF ATLANTA</p>
                                    </div>
                                </div>
                            </div>
                            <div className="swiper-pagination" />
                        </div>
                    </div>
                </div>
                <div className="section_9 d-flex align-items-center">
                    <div className="container">
                        <div className="text-content">
                            <h2 className="title text-golden">IT'S THE FIGHT OF YOUR LIFE. NEED ROBERT IN YOUR CORNER?</h2>
                            <a href="#" className="btn btn-outline">Contact us</a>
                        </div>
                    </div>
                </div>
            </div>

        </>
    )
}
