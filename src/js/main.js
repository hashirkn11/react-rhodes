var swiper = new Swiper(".section_5 .mySwiper", {
    slidesPerView: 1,
    spaceBetween: 10,
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },
    // breakpoints: {
    //   640: {
    //     slidesPerView: 2,
    //     spaceBetween: 20,
    //   },
    //   768: {
    //     slidesPerView: 4,
    //     spaceBetween: 40,
    //   },
    //   1024: {
    //     slidesPerView: 5,
    //     spaceBetween: 50,
    //   },
    // },
  });

  var swiper = new Swiper(".section_8 .mySwiper", {
    slidesPerView: "auto",
    spaceBetween: 49,
    pagination: {
      el: ".swiper-pagination",
      clickable: true,
    },
    // breakpoints: {
    //   640: {
    //     slidesPerView: 2,
    //     spaceBetween: 20,
    //   },
    //   768: {
    //     slidesPerView: 4,
    //     spaceBetween: 40,
    //   },
    //   1024: {
    //     slidesPerView: 5,
    //     spaceBetween: 50,
    //   },
    // },
  });


  let video = document.querySelector('.section_7 .video');
  let play_btn = document.querySelector('.play_video');
  let close_btn = document.querySelector('.close_video');
  play_btn.addEventListener('click', function(e) {
    e.preventDefault()
    video.classList.add('active')
  })
  close_btn.addEventListener('click', function(e) {
    e.preventDefault()
    video.classList.remove('active')
  })