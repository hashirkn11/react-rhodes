import {React, useState, useEffect} from 'react';

export const Story = ({title}) => {
    let [Dtitle, setTitle] = useState('')
    document.querySelector('title').textContent = Dtitle;
    useEffect(() => {
        setTitle(title)
    })
    return (
        <div>
            <div className="blog_header flex-center sm-header">
                <h1 className="hero_title">success stories</h1>
            </div>
            <main className="blog_main">
                <div className="container">
                    <span className="posted_date">August 11, 2017</span>
                    <h2 className="title">Prime Healthcare, its founder, and doctor pay $37.5M in whistleblower kickbacks, Stark
                        case</h2>
                    <div className="row justify-content-between">
                        <div className="col-md-5">
                            <div className="blog_img">
                                <img src="img/blog_img.png" alt="img" />
                            </div>
                        </div>
                        <div className="col-md-6">
                            <div className="text-content">
                                <p className="para">As always, your consultation is confidential and privileged, but please be aware
                                    that no attorney-client relationship is yet formed by this exchange of information. In other
                                    words, we will provide you with a free evaluation of your claim, but we do not represent you
                                    yet, even if you send in the information. Should we feel that we can successfully prosecute
                                    a claim on your behalf, we will provide you with a written engagement letter outlining the
                                    terms of the representation.
                                    PHH added that the settlement agreements cover mortgage loans insured by the Federal Housing
                                    Administration (“FHA”) during the period between Jan. 1, 2006 and Dec. 31, 2011, some
                                    mortgage loans insured by the VA, and mortgage loans sold to Freddie Mac and Fannie Mae.
                                </p>
                                <p className="para">Much of the information covered by the settlement was provided by an
                                    employee-turned-whistleblower.</p>
                            </div>
                        </div>
                    </div>
                    <p className="para">The False Claims Act was passed in response to fraud on President Abraham Lincoln’s Union
                        army during the U.S. civil war. Congress and taxpayers alike were outraged by businesses and
                        “pork-barrel” businessmen that profited handsomely from selling American civil war soldiers rotten food,
                        boots with holes in the soles, and guns that could not fire.
                    </p>
                    <p className="para">The statute is unique amongst federal and state civil fraud remedies, as it empowers
                        individuals with “inside” information about fraud on the government to share in any recovery the
                        government may make. These individuals, referred to as “relators” frequently obtain 15-30% of recoveries
                        made because of their FCA lawsuits.
                    </p>
                    <p className="para">Mary Bozzelli, a former employee of PHH, filed a lawsuit under the qui tam whistleblower
                        provisions of the False Claims Act. Bozzelli worked for PHH from 1992 to 2011 as an underwriter and
                        underwriting supervisor. Due to her contributions to the settlement, the United States awarded Bozzelli
                        more than $9 million for the role she played in blowing the whistle on PHH’s mortgage fraud.
                    </p>
                    <p className="para">“It is great to see PHH finally held accountable for its actions,” said Bozzelli, who filed
                        the lawsuit in 2013. “Mortgage fraud is hardly victimless. Not only did PHH defraud taxpayers, but
                        instead of helping deserving borrowers obtain home loans through the government loan programs, I
                        witnessed firsthand the ways in which PHH abused the programs to line its own pockets.”
                    </p>
                    <p className="para">As part of the settlement, PHH admitted to the following concerning its origination of
                        improper FHA loans:
                    </p>
                    <p className="para">Between Jan. 1, 2006, and Dec. 31, 2011, it certified for FHA insurance mortgage loans that
                        did not meet HUD underwriting requirements and did not adhere to FHA’s self-reporting requirements.
                        Examples of loan defects that PHH admitted resulted in loans being ineligible for FHA mortgage
                        insurance.
                    </p>
                    <p className="para">In resolving the allegations, the company admitted to: (1) failing to document the
                        borrowers’ creditworthiness and verification of employment; (2) failing to document the borrower’s
                        claimed equity in previous homes; and, (3) insuring loans for FHA mortgage insurance despite the
                        borrower failing to meet HUD’s minimum statutory investment for the loan.
                    </p>
                    <p className="para">
                        In a separate admission, PHH admitted that between Jan. 1, 2006, and Dec. 31, 2011, the company did not
                        self-report any problematic loans to HUD, as required per FHA regulations.
                    </p>
                    <p className="para">PHH is one of many lenders dealing with allegations of fraud in relation to origination and
                        underwriting practices. Quicken Loans, run by billionaire businessman and Cleveland Cavaliers owner Dan
                        Gilbert, is the nation’s largest Federal Housing Administration-backed mortgage lender, and sued the DOJ
                        in April 2015 after being accused of fraudulent lending practices. That case is still pending.
                    </p>
                    <p className="para">Though mortgage fraud may not appear as clear-cut as what Bozzelli experienced at PHH, if
                        one suspects their mortgage company of fraudulent practices, consulting an experienced whistleblower
                        attorney or an attorney who specializes in the False Claims Act may help evaluate and guide a potential
                        claim.
                    </p>
                    <p className="para">Greene LLP employs a low-volume high-attention approach to complex civil litigation and
                        specializes in False Claims Act litigation. Greene LLP cases have resulted in over $1.2 billion in
                        government recoveries, including nearly $500 million for claims for which the government declined to
                        intervene.
                    </p>
                    <span className="blog_info text-golden">Posted in: Uncategorized
                    </span>
                    <span className="blog_info text-golden">Updated: August 15, 2017 3:28 pm
                    </span>
                </div>
                <div className="related_stories">
                    <div className="container">
                        <h2 className="section_title text-center">RELATED CASESTUDY </h2>
                        <div className="row">
                            <div className="col-md-6"><div className="success_story_blog">
                                <div className="img">
                                    <div className="frame" />
                                    <img src="img/success_story_img.png" alt="img" />
                                </div>
                                <h3 className="title">Prime Healthcare, its founder, and doctor pay $37.5M in whistleblower kickbacks, Stark case</h3>
                                <p className="des">Prime Healthcare, its founder and CEO, Dr. Prem Reddy, and a California cardiologist pay $37.5 million to resolve allegations of healthcare fraud. </p>
                                <a href="#" className="link">learn more</a>
                            </div></div>
                            <div className="col-md-6">
                                <div className="success_story_blog">
                                    <div className="img">
                                        <div className="frame" />
                                        <img src="img/success_story_img.png" alt="img" />
                                    </div>
                                    <h3 className="title">Prime Healthcare, its founder, and doctor pay $37.5M in whistleblower kickbacks, Stark case</h3>
                                    <p className="des">Prime Healthcare, its founder and CEO, Dr. Prem Reddy, and a California cardiologist pay $37.5 million to resolve allegations of healthcare fraud. </p>
                                    <a href="#" className="link">learn more</a>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </main>
        </div>

    )
}
